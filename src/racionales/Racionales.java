/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package racionales;

/**
 *
 * @author sofie_000
 */
public class Racionales {
    //Variable que representa al numerador
    int p;
    //Variable que representa al denominador
    int q;
    
    /*
    *Constructor (Sin parametros)
    */
    Racionales(){
        this.p = 1;
        this.q = 1;
    }
    /*
    *Constructor (Con parametros)
    */
    Racionales(int p, int q){
        this.p = p;
        this.q = q;
    }
    /**
     * Metodo que asigna un nuevo valor al numerador
     * @param p El nuevo valor del numerador
     */

    private void setNumerador(int p){
        this.p = p;
    }
    /**
     * Método que regresa el valor del denominador
     * @param q El nuevo valor del denominador
     */
    private void setDenominador(int q){
        this.q = q;
    }
    
    /**
     * Método que regresa el valor del numerador
     * @return El valor del numerador
     */
    private int getNumerador(){
        return this.p;
    }
    /**
     * Método que regresa el valor del denominador
     * @return El valor del denominador
     */
    private int getDenominador(){
        return this.q;
    }
    
    /**
     * Método que imprime el numero racional
     */
    void show(){
        System.out.println(getNumerador() + "/" + getDenominador());
    }
    /**
     * Metodo que realiza la división de dos numeros racionales
     * @param r El numero racional con el que dividiremos
     * @return El resultado de la división
     */
    Racionales divideRacionales(Racionales r){
        int p = 0;
        int q = 1;
        
        p = getNumerador()* r.getDenominador();
        q = getDenominador()* r.getNumerador();
        
        return (new Racionales(p,q));
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // creamos dos objetos que representan a dos numeros racionales
        Racionales r1 = new Racionales (1,2);
        Racionales r2 = new Racionales (2,3);
        //Declaramos un objeto
        Racionales r3;
        
        System.out.println("R1 es: ");
        r1.show();
        
        System.out.println("R2 es: ");
        r2.show();
        
        //En r3 guardaremos el resultado
        //DIVISIÓN:
        r3 = r1.divideRacionales(r2);
        
        System.out.println("El resultado de la división es: ");
        r3.show();
        
    }
    
}
